<?php

declare(strict_types=1);

namespace MauticPlugin\MindsBundle\EventListener;

use Mautic\CampaignBundle\CampaignEvents;
use Mautic\CampaignBundle\Event\CampaignBuilderEvent;
use Mautic\CampaignBundle\Event\CampaignExecutionEvent;
use MauticPlugin\MindsBundle\Form\Type\SupermindOfferType;
use MauticPlugin\MindsBundle\MindsEvents;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class CampaignSubscriber implements EventSubscriberInterface
{
    private LoggerInterface $logger;

    private const SUPERMIND_OFFER_ENDPOINT = 'api/v3/supermind/bulk';
    private const MAUTIC_MINDS_HEADER      = 'X-Mautic-Shared-Secret-Header';

    private const MAUTIC_MINDS_SHARED_SECRET_NAME = 'MAUTIC_MINDS_SHARED_SECRET';

    public function __construct(
        LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            CampaignEvents::CAMPAIGN_ON_BUILD => ['onCampaignBuild', 0],
            MindsEvents::SEND_SUPERMIND_OFFER => ['onSendSupermindOffer', 0],
        ];
    }

    public function onCampaignBuild(CampaignBuilderEvent $event): void
    {
        $event->addAction(
            'minds.send_supermind_offer',
            [
                'eventName'          => MindsEvents::SEND_SUPERMIND_OFFER,
                'label'              => 'Send Supermind Offer',
                'description'        => 'Send Supermind Offer',
                'formType'           => SupermindOfferType::class,
                'anchorRestrictions' => [
                    'source.bottom',
                ],
            ]
        );
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function onSendSupermindOffer(CampaignExecutionEvent $event): void
    {
        $properties            = $event->getEvent()['properties'];
        $supermindOfferDetails = [
            'source_activity' => $properties['source_activity'],
            'payment_amount'  => $properties['payment_amount'],
            'payment_type'    => $properties['payment_type'],
            'reply_type'      => $properties['reply_type'],
        ];

        $supermindOfferDetails['receiver_guid'] = $event->getLead()->getField('guid', 'core')['value'];

        $this->logger->info('Sending Supermind Offer to Minds Engine', $supermindOfferDetails);

        $curlClient = new CurlHttpClient();
        $curlClient->request(
            'POST',
            getenv('MAUTIC_MINDS_ENGINE_URL').self::SUPERMIND_OFFER_ENDPOINT,
            [
                'headers' => [
                    self::MAUTIC_MINDS_HEADER => getenv(self::MAUTIC_MINDS_SHARED_SECRET_NAME),
                    'Accept'                  => '*/*',
                ],
                'body' => json_encode($supermindOfferDetails),
            ]
        );
    }
}
