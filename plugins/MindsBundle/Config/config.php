<?php

return [
    'name'        => 'Minds',
    'description' => 'Enables integration with Minds.',
    'version'     => '1.0',
    'author'      => 'Minds Inc',
    'services'    => [
        'events' => [
            'plugin.mindsbundle.campaignsubscriber' => [
                'class'     => \MauticPlugin\MindsBundle\EventListener\CampaignSubscriber::class,
                'arguments' => [
                    'logger',
                ],
            ],
        ],
        'forms' => [
            'minds.integration.type.supermind_offer' => [
                'class' => \MauticPlugin\MindsBundle\Form\Type\SupermindOfferType::class,
            ],
        ],
        'integrations' => [
            'minds.integration' => [
                'class' => \MauticPlugin\MindsBundle\Integration\MindsIntegration::class,
                // 'arguments' => [
                //     'event_dispatcher',
                //     'mautic.helper.cache_storage',
                //     'doctrine.orm.entity_manager',
                //     'session',
                //     'request_stack',
                //     'router',
                //     'translator',
                //     'logger',
                //     'mautic.helper.encryption',
                //     'mautic.lead.model.lead',
                //     'mautic.lead.model.company',
                //     'mautic.helper.paths',
                //     'mautic.core.model.notification',
                //     'mautic.lead.model.field',
                //     'mautic.plugin.model.integration_entity',
                //     'mautic.lead.model.dnc',
                // ],
                'tags' => [
                    'mautic.basic_integration',
                ],
            ],
        ],
    ],
];
