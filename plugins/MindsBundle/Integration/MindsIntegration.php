<?php

declare(strict_types=1);

namespace MauticPlugin\MindsBundle\Integration;

use Mautic\IntegrationsBundle\Integration\BasicIntegration;

class MindsIntegration extends BasicIntegration
{
    private const NAME         = 'Minds';
    private const DISPLAY_NAME = 'Minds Inc.';
    private const ICON         = 'plugins/MindsBundle/Assets/img/mindsBulb.png';

    public function getName(): string
    {
        return self::NAME;
    }

    public function getDisplayName(): string
    {
        return self::DISPLAY_NAME;
    }

    public function getIcon(): string
    {
        return self::ICON;
    }
}
