<?php

declare(strict_types=1);

namespace MauticPlugin\MindsBundle;

use Mautic\IntegrationsBundle\Bundle\AbstractPluginBundle;

class MindsBundle extends AbstractPluginBundle
{
}
