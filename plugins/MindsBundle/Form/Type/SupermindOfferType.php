<?php

declare(strict_types=1);

namespace MauticPlugin\MindsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class SupermindOfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('source_activity', TextType::class, [
            'label'    => 'mautic.minds.supermind_offer.source_activity',
            'required' => true,
            'attr'     => [
                'class'       => 'form-control',
                'placeholder' => 'mautic.minds.supermind_offer.source_activity.placeholder',
            ],
        ]);

        $builder->add('payment_amount', NumberType::class, [
            'label'    => 'mautic.minds.supermind_offer.payment_amount',
            'required' => true,
            'attr'     => [
                'class'       => 'form-control',
                'placeholder' => 'mautic.minds.supermind_offer.payment_amount.placeholder',
            ],
        ]);

        $builder->add('payment_type', ChoiceType::class, [
            'choices' => [
                'mautic.minds.supermind_offer.payment_type.cash'            => 0,
                'mautic.minds.supermind_offer.payment_type.offchain_tokens' => 1,
            ],
            'label'       => 'mautic.minds.supermind_offer.payment_type',
            'required'    => true,
            'placeholder' => 'mautic.minds.supermind_offer.payment_type.placeholder',
            'constraints' => [
                new NotBlank([
                    'message' => 'mautic.minds.supermind_offer.payment_type.validation.not_blank',
                ]),
            ],
        ]);

        $builder->add('reply_type', ChoiceType::class, [
            'choices' => [
                'mautic.minds.supermind_offer.reply_type.text'  => 0,
                'mautic.minds.supermind_offer.reply_type.image' => 1,
                'mautic.minds.supermind_offer.reply_type.video' => 2,
            ],
            'label'    => 'mautic.minds.supermind_offer.reply_type',
            'required' => true,
        ]);
    }
}
