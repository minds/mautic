<?php

declare(strict_types=1);

namespace MauticPlugin\MindsBundle;

final class MindsEvents
{
    public const SEND_SUPERMIND_OFFER = 'minds.send_supermind_offer';
}
