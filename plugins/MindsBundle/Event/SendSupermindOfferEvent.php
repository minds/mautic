<?php
declare(strict_types=1);

namespace MauticPlugin\MindsBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;

class SendSupermindOfferEvent extends Event
{
}